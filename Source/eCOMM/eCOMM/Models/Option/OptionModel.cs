﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using static eCOMM.CustomHelpers;

namespace eCOMM.Models.Option
{
    public class OptionModel
    {
        public OptionModel()
        {
            Values = new List<OptionValueModel>();
            DisplayTypes = new List<CustomSelectListItem>();
        }

        public int ID { get; set; }
        [Display(Name = "Option Name")]
        public string OptionName { get; set; }
        [Display(Name = "Display Name")]
        public string DisplayName { get; set; }
        [Display(Name = "Display Type")]
        public int DisplayTypeID { get; set; }
        public string DefaultValue { get; set; }
        public bool? DefaultChecked { get; set; }

        public IEnumerable<OptionValueModel> Values { get; set; }
        public IEnumerable<CustomSelectListItem> DisplayTypes { get; set; }
    }
}
