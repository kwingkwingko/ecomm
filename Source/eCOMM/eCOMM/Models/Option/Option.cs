﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCOMM.Models.Option
{
    public class Option
    {
        public int ID { get; set; }
        public string OptionName { get; set; }
        public string DisplayName { get; set; }
        public int DisplayTypeID { get; set; }
        public string DefaultValue { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
