﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCOMM.Models.Option
{
    public class OptionValueModel
    {
        public int ID { get; set; }
        public int OptionID { get; set; }
        public string Name { get; set; }
        public bool MakeDefault { get; set; }
        public int SortOrder { get; set; }
    }
}
