﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eCOMM.Models.Account
{
    public class ForgotPassModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
        public bool IsSuccess { get; set; }
    }
}