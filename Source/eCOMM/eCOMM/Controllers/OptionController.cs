﻿using eCOMM.Models.Option;
using eCOMM.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using static eCOMM.CustomHelpers;

namespace eCOMM.Controllers
{
    [Authorize]
    public class OptionController : Controller
    {
        private readonly OptionRepository _optionRepository;
        private readonly UserRepository _userRepository;
        private readonly DisplayTypeRepository _displayTypeRepository;

        public OptionController()
        {
            string connStr = ConfigurationManager.ConnectionStrings["eCOMMConnection"].ToString();
            _optionRepository = new OptionRepository(connStr);
            _userRepository = new UserRepository(connStr);
            _displayTypeRepository = new DisplayTypeRepository(connStr);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetOptions()
        {
            var user = _userRepository.GetUserByEmail(User.Identity.Name);
            var result = _optionRepository.GetOptions(user.Id);

            return Json(new
            {
                data = result.Select(x => new string[]
                {
                    x.OptionName,
                    x.DisplayName,
                    x.ID.ToString()
                }).ToList()
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var viewModel = new OptionModel();
            viewModel.DisplayTypes = GetDisplayTypes();
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(OptionModel formModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var currentUser = _userRepository.GetUserByEmail(User.Identity.Name);
                    var option = new Option
                    {
                        OptionName = formModel.OptionName,
                        DisplayName = formModel.DisplayName,
                        DisplayTypeID = formModel.DisplayTypeID,
                        DefaultValue = formModel.DefaultValue ?? string.Empty,
                        CreatedBy = currentUser.Id,
                        CreatedDate = DateTime.Now,
                        ModifiedBy = currentUser.Id,
                        ModifiedDate = DateTime.Now
                    };

                    _optionRepository.Create(option);
                    return RedirectToAction("Index", "Option");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    formModel.DisplayTypes = GetDisplayTypes();
                    return View(formModel);
                }
            }
            formModel.DisplayTypes = GetDisplayTypes();
            return View(formModel);
        }

        private List<CustomSelectListItem> GetDisplayTypes()
        {
            var displayTypes = _displayTypeRepository.Get();
            var result = displayTypes.Select(x =>
                new CustomSelectListItem
                {
                    Text = x.Name,
                    Code = x.Code,
                    Value = x.ID.ToString()
                }).ToList();
            return result;
        }
    }
}
