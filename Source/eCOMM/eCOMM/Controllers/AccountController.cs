﻿using eCOMM.Helpers;
using eCOMM.Models.Account;
using eCOMM.Repositories;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace eCOMM.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserRepository _userRepository;

        public AccountController()
        {
            string connStr = ConfigurationManager.ConnectionStrings["eCOMMConnection"].ToString();
            _userRepository = new UserRepository(connStr);
        }

        [Authorize]
        public ActionResult Index()
        {
            var viewModel = _userRepository.GetPersonList();
            return View(viewModel);
        }

        public ActionResult Register()
        {
            var viewModel = new RegistrationModel();
            return View(viewModel);
        }

        public ActionResult Login()
        {
            var viewModel = new LoginModel();
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Login(LoginModel formModel)
        {
            if (ModelState.IsValid)
            {
                var user = _userRepository.GetUserByEmail(formModel.Email);

                if (user != null)
                {
                    if (user.Password == Helper.Hash(formModel.Password) && user.IsVerified)
                    {
                        var ctx = Request.GetOwinContext();
                        var authenticationManager = ctx.Authentication;

                        var claims = new List<Claim>();
                        claims.Add(new Claim(ClaimTypes.Name, user.Email));
                        claims.Add(new Claim(ClaimTypes.Sid, user.Id.ToString()));
                        claims.Add(new Claim(ClaimTypes.NameIdentifier, "userId"));
                        claims.Add(new Claim(ClaimTypes.Thumbprint, user.PhysicalFileName));
                        var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
                        authenticationManager.SignIn(identity);
                        return RedirectToAction("Index", "Home");
                    }
                }
            }
            if(ModelState.Values != null && !ModelState.Values.Any(x => x.Errors != null && x.Errors.Count > 0))
                ModelState.AddModelError("", "Invalid Username or Password");
            return View(); ;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegistrationModel formModel)
        {
            ModelState.Clear();
            try
            {
                if (ModelState.IsValid)
                {
                    if (!formModel.Password.Equals(formModel.ConfirmPassword))
                    {
                        ModelState.AddModelError("", "Passwords are not the same");
                    }
                    else
                    {
                        formModel.Password = Helper.Hash(formModel.Password);
                        formModel.CreatedBy = "user";
                        formModel.CreatedDate = DateTime.Now;
                        var retValue = _userRepository.Create(formModel);

                        // Send email notification if successful
                        if (retValue == Helpers.Constants.Num_One)
                        {
                            UrlHelper urlHelper = new UrlHelper(this.ControllerContext.RequestContext);
                            string scheme = urlHelper.RequestContext.HttpContext.Request.Url.Scheme;
                            var verifyUrl = this.Url.Action("VerifyEmail", "Account", new { email = formModel.Email}, scheme);

                            ListDictionary bodyReplacements = new ListDictionary();
                            bodyReplacements.Add("{email}", formModel.Email);
                            bodyReplacements.Add("{verifyUrl}", verifyUrl);
                            await Task.Run(() => EmailHelper.SendEmail(formModel.Email, Helpers.Constants.Confirm_Email_Subject, bodyReplacements, Helpers.Constants.Confirm_Email_Template)); 
                        }

                        formModel.IsSuccess = true;
                        return View(formModel);
                    }
                }
                return View(formModel);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "An error occured");
                return View(formModel);
            }
        }

        [Authorize]
        public ActionResult Profile(int? id)
        {
            if (id.HasValue && id.Value > 0)
            {
                var viewModel = _userRepository.Get(id.Value);
                return View(viewModel);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Profile(ProfileModel formModel)
        {
            ModelState.Clear();
            try
            {
                if (ModelState.IsValid)
                {
                    // if new file is uploaded, update the filename
                    if(Request.Files["file"] != null && Request.Files["file"].ContentLength > 0)
                    {
                        var newFile = Request.Files["file"];
                        formModel.PhysicalFileName = !string.IsNullOrEmpty(formModel.PhysicalFileName) ?
                            formModel.PhysicalFileName : Guid.NewGuid().ToString() + Path.GetExtension(newFile.FileName);

                        var path = Server.MapPath("~/Files/Images/") + formModel.PhysicalFileName;
                        newFile.SaveAs(path);
                    }
                    formModel.ModifiedBy = "user";
                    formModel.ModifiedDate = DateTime.Now;
                    formModel.IsSuccess = true;
                    _userRepository.UpdateUser(formModel);
                    return View(formModel);
                }

                ModelState.AddModelError("", "Unable to update user");
                return View(formModel);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "An error occured");
                return View(formModel);
            }
        }

        [Authorize]
        public ActionResult ChangePassword(int? id)
        {
            if (id.HasValue && id.Value > 0)
            {
                var viewModel = new ChangePasswordModel { Id = id.Value };
                return View(viewModel); 
            }

            return HttpNotFound();
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangePasswordModel formModel)
        {
            ModelState.Clear();
            if (ModelState.IsValid)
            {
                var user = _userRepository.Get(formModel.Id);
                if(user != null)
                {
                    if (!user.Password.Equals(Helper.Hash(formModel.OldPassword)))
                    {
                        ModelState.AddModelError("", "Incorrect Password");
                        return View(formModel);
                    }
                    else
                    {
                        try
                        {
                            formModel.NewPassword = Helper.Hash(formModel.NewPassword);
                            _userRepository.ChangePassword(formModel);
                            formModel.IsSuccess = true;
                            return View(formModel);
                        }
                        catch { }
                    }
                }
            }

            return View(formModel);
        }


        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> ForgotPassword(ForgotPassModel formModel)
        {
            ModelState.Clear();
            if (!String.IsNullOrEmpty(formModel.Email))
            {
                var tempPassword = Membership.GeneratePassword(8, 4);
                var user = _userRepository.GetUserByEmail(formModel.Email);

                if (user != null)
                {
                    try
                    {
                        var changePassModel = new ChangePasswordModel
                        {
                            Id = user.Id,
                            NewPassword = Helper.Hash(tempPassword)
                        };

                        _userRepository.ChangePassword(changePassModel);

                        //Send email
                        ListDictionary bodyReplacements = new ListDictionary();
                        bodyReplacements.Add("{email}", formModel.Email);
                        bodyReplacements.Add("{password}", tempPassword);
                        await Task.Run(() => EmailHelper.SendEmail(formModel.Email, Helpers.Constants.Forgot_Pass_Subject, bodyReplacements, Helpers.Constants.Forgot_Pass_Template));
                        formModel.IsSuccess = true;
                        return View(formModel);
                    }
                    catch (Exception ex)
                    {
 
                    }
                }
            }

            return View(formModel);
        }

        [Authorize]
        public ActionResult Logout()
        {
            var ctx = Request.GetOwinContext();
            var authenticationManager = ctx.Authentication;
            authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", "Account");
        }

        public ActionResult VerifyEmail(string email)
        {
            if (!String.IsNullOrEmpty(email))
            {
                var isVerified = _userRepository.VerifyUser(email);

                if (isVerified == 1)
                {
                    return RedirectToAction("Index", "Home");
                }
            }

            return RedirectToAction("Index", "Home");
        }
    }
}
