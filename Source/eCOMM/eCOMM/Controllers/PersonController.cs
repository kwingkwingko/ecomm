﻿using eCOMM.Models;
using eCOMM.Models.Person;
using eCOMM.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCOMM.Controllers
{
    public class PersonController : Controller
    {
        private readonly PersonRepository _personRepository;

        public PersonController()
        {
            string connStr = ConfigurationManager.ConnectionStrings["eCOMMConnection"].ToString();
            _personRepository = new PersonRepository(connStr);
        }

        //
        // GET: /Person/
        public ActionResult Index()
        {
            var model = _personRepository.GetPersonList();
            return View(model);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(PersonModel person)
        {
            if (ModelState.IsValid)
            {
                person.CreatedBy = "dummyUser";
                person.CreatedDate = DateTime.Now;
                _personRepository.CreatePerson(person);
                return RedirectToAction("Index");
            }
            return RedirectToAction("Create");
        }

        public ActionResult Edit(int id)
        {
            var person = _personRepository.GetPersonById(id);
            return View(person);
        }

        [HttpPost]
        public ActionResult Edit(PersonModel person)
        {
            if (ModelState.IsValid)
            {
                person.ModifiedBy = "dummyModifier";
                person.ModifiedDate = DateTime.Now;
                _personRepository.UpdatePerson(person);
                return RedirectToAction("Index");
            }
            return RedirectToAction("Edit", new { id = person.Id });
        }

        public ActionResult Detail(int id)
        {
            var person = _personRepository.GetPersonById(id);
            return View(person);
        }

        public ActionResult Delete(int id)
        {
            _personRepository.DeletePerson(id);
            return RedirectToAction("Index");
        }
	}
}