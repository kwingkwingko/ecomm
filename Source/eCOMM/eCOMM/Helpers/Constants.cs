﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCOMM.Helpers
{
    public static class Constants
    {
        public const int Num_One = 1;
        public const string Confirm_Email_Template = "Confirm_Email";
        public const string Confirm_Email_Subject = "Email Confirmation";
        public const string Forgot_Pass_Template = "Forgot_Pass";
        public const string Forgot_Pass_Subject = "Forgot Password";
    }

    public static class Dropdowns
    {
        public static List<SelectListItem> YesNo = new List<SelectListItem>
        {
            new SelectListItem { Text = "Yes", Value = "true" },
            new SelectListItem { Text = "No", Value = "false" },
        };
    }
}