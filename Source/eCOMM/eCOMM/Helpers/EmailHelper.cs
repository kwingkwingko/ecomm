﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI.WebControls;

namespace eCOMM.Helpers
{
    public static class EmailHelper
    {
        public static async void SendEmail(string to, string subject, ListDictionary replacements, string templateName)
        {
            var sendGrid = new SendGridMailHelper();
            
            var md = new MailDefinition();
            md.From = new MailAddress(ConfigurationManager.AppSettings["mailDomain"], "Weed Angel").ToString();
            md.BodyFileName = "~/EmailTemplates/"+ templateName +".html";
            md.Subject = subject;
            md.IsBodyHtml = true;
            
            string sTime = DateTime.Now.AddMinutes(1).ToString("dd MMM yyyy") + DateTime.Now.ToShortTimeString();

            MailMessage msg = md.CreateMailMessage(to, replacements, new System.Web.UI.UserControl{AppRelativeVirtualPath = "~/"});
            msg.Headers.Add("expiry-date", sTime);
            await sendGrid.SendAsync(msg);
        }
    }
}