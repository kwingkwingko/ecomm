﻿using eCOMM.Models.Option;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCOMM.Repositories
{
    public class DisplayTypeRepository
    {
        private DataAccessLayer dataAccessLayer;

        public DisplayTypeRepository(string connStr)
        {
            dataAccessLayer = new DataAccessLayer(connStr);
        }

        public List<DisplayType> Get()
        {
            return dataAccessLayer.GetEntityList<DisplayType>("dbo.GetDisplayTypes", new { });
        }

        public DisplayType Get(int id)
        {
            return dataAccessLayer.GetEntity<DisplayType>("dbo.GetDisplayType", new { Id = id });
        }
    }
}
