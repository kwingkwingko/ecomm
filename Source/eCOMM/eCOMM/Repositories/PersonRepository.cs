﻿using eCOMM.Models.Person;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace eCOMM.Repositories
{
    public class PersonRepository
    {
        private DataAccessLayer dataAccessLayer;

        public PersonRepository(string connStr)
        {
            dataAccessLayer = new DataAccessLayer(connStr);
        }

        public List<PersonModel> GetPersonList()
        {
            return dataAccessLayer.GetEntityList<PersonModel>("dbo.GetPersonList", new { });
        }

        public PersonModel GetPersonById(int id)
        {
            return dataAccessLayer.GetEntity<PersonModel>("dbo.GetPersonById", new  { PersonId = id});
        }

        public void CreatePerson(PersonModel person)
        {
            dataAccessLayer.GetSingleValue_String("dbo.CreatePerson", person);
        }

        public void UpdatePerson(PersonModel person)
        {
            dataAccessLayer.GetSingleValue_String("dbo.UpdatePerson", person);
        }

        public void DeletePerson(int id)
        {
            string connStr = ConfigurationManager.ConnectionStrings["eCOMMConnection"].ToString();
            using (var conn = new SqlConnection(connStr))
            {
                using (var cmd = new SqlCommand("dbo.DeletePerson"))
                {
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@PersonId", SqlDbType.Int).Value = id;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}