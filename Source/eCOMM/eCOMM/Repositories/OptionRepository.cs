﻿using eCOMM.Models.Option;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCOMM.Repositories
{
    public class OptionRepository
    {
        private DataAccessLayer dataAccessLayer;

        public OptionRepository(string connStr)
        {
            dataAccessLayer = new DataAccessLayer(connStr);
        }

        public List<Option> GetOptions(int userID)
        {
            return dataAccessLayer.GetEntityList<Option>("dbo.GetOptions", new { ID = userID });
        }

        public Option Get(int id)
        {
            return dataAccessLayer.GetEntity<Option>("dbo.GetOption", new { Id = id });
        }

        public int Create(Option option)
        {
            return dataAccessLayer.GetSingleValue_Int("dbo.CreateOption", option);
        }

        public void Update(Option option)
        {
            dataAccessLayer.GetSingleValue_String("dbo.UpdateOption", option);
        }
    }
}
