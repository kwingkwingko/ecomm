﻿using eCOMM.Models.Account;
using eCOMM.Models.Person;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace eCOMM.Repositories
{
    public class UserRepository
    {
        private DataAccessLayer dataAccessLayer;

        public UserRepository(string connStr)
        {
            dataAccessLayer = new DataAccessLayer(connStr);
        }

        public List<UserListViewModel> GetPersonList()
        {
            return dataAccessLayer.GetEntityList<UserListViewModel>("dbo.GetUserList", new { });
        }

        public ProfileModel Get(int id)
        {
            return dataAccessLayer.GetEntity<ProfileModel>("dbo.GetUserById", new { Id = id });
        }

        public ProfileModel GetUserByEmail(string email)
        {
            return dataAccessLayer.GetEntity<ProfileModel>("dbo.GetUserByEmail", new { Email = email });
        }

        public int Create(RegistrationModel user)
        {
            return dataAccessLayer.GetSingleValue_Int("dbo.CreateUser", user);
        }

        public void UpdateUser(ProfileModel user)
        {
            dataAccessLayer.GetSingleValue_String("dbo.UpdateUser", user);
        }

        public int ChangePassword(ChangePasswordModel user)
        {
            return dataAccessLayer.GetSingleValue_Int("dbo.ChangePassword", user);
        }

        public int VerifyUser(string email)
        {
            var data = new { Email = email };
            return dataAccessLayer.GetSingleValue_Int("dbo.VerifyUser", data);
        }
    }
}