﻿$(function(){
	"use strict"
	var type = "";

	$('.to-labelauty-icon').labelauty({
	    label: false
	});

	$('.MultipleChoice,.Color').sortable({
	    update: function (event, ui) {
	        var test = $(this).sortable('toArray').toString();
	        console.log(test);
	    }
	});

	$('.color-picker').colorpicker();

	$('#DisplayTypeID').on('change', function () {
	    var displayType = $(this);
	    type = displayType.find('option[value="' + displayType.val() + '"]').data('code');
	    $('.type-option').each(function () {

	        // display option
	        if ($(this).hasClass(type))
	            $(this).removeClass("hidden");
	        else {
	            $(this).addClass("hidden");
	            var items = $(this).find('.item:not(.hidden)');
	            items.each(function () {
	                $(this).remove();
	            })
	        }

            // add button
	        if (type == 'MultipleChoice' || type == "Color")
	            $('.add').removeClass('hidden');
            else
	            $('.add').addClass('hidden');
	    });
	});
	$('#DisplayTypeID').change();

	$('.add').on('click', function () {
	    var container = $('.' + type + '.type-option');
	    var row = container.find('.row.hidden.item');

	    var newRow = row.clone();
	    newRow.removeClass('hidden');
	    newRow.appendTo(container);

	    if(type == "Color")
	        $('.color-picker').colorpicker();

	});

	$('.type-option').on('click', '.remove', function () {
	    var container = $(this).closest('.item');
	    container.remove();
	});


});