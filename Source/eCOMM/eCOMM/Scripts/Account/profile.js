﻿$(function () {
    $('#uploadBtn').on('click', function () {
        $('#file').click();
    });

    $('#file').on('change', function () {
        if (this.files && this.files[0]) {
            $('#FileName').val(this.files[0].name)
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#profileImage').attr('src', e.target.result);
            };
            reader.readAsDataURL(this.files[0]);
        }
    });
});