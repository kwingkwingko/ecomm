USE [master]
GO
/****** Object:  Database [eCOMM]    Script Date: 10/15/2016 12:59:58 AM ******/
CREATE DATABASE [eCOMM]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'eCOMM', FILENAME = N'C:\Users\Justine\eCOMM.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'eCOMM_log', FILENAME = N'C:\Users\Justine\eCOMM_log.ldf' , SIZE = 3456KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [eCOMM] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [eCOMM].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [eCOMM] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [eCOMM] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [eCOMM] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [eCOMM] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [eCOMM] SET ARITHABORT OFF 
GO
ALTER DATABASE [eCOMM] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [eCOMM] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [eCOMM] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [eCOMM] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [eCOMM] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [eCOMM] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [eCOMM] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [eCOMM] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [eCOMM] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [eCOMM] SET  DISABLE_BROKER 
GO
ALTER DATABASE [eCOMM] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [eCOMM] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [eCOMM] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [eCOMM] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [eCOMM] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [eCOMM] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [eCOMM] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [eCOMM] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [eCOMM] SET  MULTI_USER 
GO
ALTER DATABASE [eCOMM] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [eCOMM] SET DB_CHAINING OFF 
GO
ALTER DATABASE [eCOMM] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [eCOMM] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [eCOMM] SET DELAYED_DURABILITY = DISABLED 
GO
USE [eCOMM]
GO
/****** Object:  Table [dbo].[Person]    Script Date: 10/15/2016 12:59:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Person](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Age] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 10/15/2016 12:59:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NULL,
	[FirstName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[Password] [varchar](100) NOT NULL,
	[Phone] [varchar](50) NOT NULL,
	[HomePhone] [varchar](50) NULL,
	[MobilePhone] [varchar](50) NULL,
	[Fax] [varchar](50) NULL,
	[ReceiveUpdates] [bit] NULL,
	[FileName] [varchar](100) NULL,
	[PhysicalFileName] [varchar](100) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Table_U1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[CreatePerson]    Script Date: 10/15/2016 12:59:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CreatePerson]
	-- Add the parameters for the stored procedure here
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),
	@Age INT,
	@CreatedBy VARCHAR(50),
	@CreatedDate Datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert new person record
    INSERT INTO Person
    (
		FirstName,
		LastName,
		Age,
		CreatedBy,
		CreatedDate
    )
    VALUES
    (
		@FirstName,
		@LastName,
		@Age,
		@CreatedBy,
		@CreatedDate
    )
    
	
END

GO
/****** Object:  StoredProcedure [dbo].[CreateUser]    Script Date: 10/15/2016 12:59:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CreateUser]
	-- Add the parameters for the stored procedure here
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),
	@Email VARCHAR(50),
	@Password VARCHAR(100),
	@Phone VARCHAR(50),
	@CreatedBy VARCHAR(50),
	@CreatedDate Datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert new user record
    INSERT INTO [User]
    (
		[FirstName],
		[LastName],
		[Email],
		[Password],
		[Phone],
		[CreatedBy],
		[CreatedDate]
    )
    VALUES
    (
		@FirstName,
		@LastName,
		@Email,
		@Password,
		@Phone,
		@CreatedBy,
		@CreatedDate
    )
    
	
END

GO
/****** Object:  StoredProcedure [dbo].[DeletePerson]    Script Date: 10/15/2016 12:59:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeletePerson]
	@PersonId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DELETE 
    FROM Person
	WHERE id = @PersonId
END

GO
/****** Object:  StoredProcedure [dbo].[GetPersonById]    Script Date: 10/15/2016 12:59:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetPersonById]
	-- Add the parameters for the stored procedure here
	@PersonId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT Id
		   ,FirstName
		   ,LastName
		   ,Age
		   ,CreatedBy
		   ,CreatedDate
	FROM Person
	WHERE Id = @PersonId
END

GO
/****** Object:  StoredProcedure [dbo].[GetPersonList]    Script Date: 10/15/2016 12:59:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetPersonList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Id
		   ,FirstName
		   ,LastName
		   ,Age
		   ,CreatedBy
		   ,CreatedDate
	FROM Person
END

GO
/****** Object:  StoredProcedure [dbo].[GetUserById]    Script Date: 10/15/2016 12:59:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetUserById]
	-- Add the parameters for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT Id
		   ,FirstName
		   ,LastName
		   ,Email
		   ,Title
		   ,Phone
		   ,HomePhone
		   ,MobilePhone
		   ,Fax
		   ,ReceiveUpdates
		   ,FileName
		   ,PhysicalFileName
	FROM [User]
	WHERE Id = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[GetUserList]    Script Date: 10/15/2016 12:59:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetUserList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Id
		   ,FirstName
		   ,LastName
		   ,Email
		   ,Phone
	FROM [User]
END

GO
/****** Object:  StoredProcedure [dbo].[UpdatePerson]    Script Date: 10/15/2016 12:59:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdatePerson]
	-- Add the parameters for the stored procedure here
	@Id INT,
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),
	@Age INT,
	@ModifiedBy VARCHAR(50),
	@ModifiedDate DateTime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE Person 
	SET FirstName = @FirstName
	    ,LastName = @LastName
	    ,Age = @Age
	    ,ModifiedBy = @ModifiedBy
	    ,ModifiedDate = @ModifiedDate
	Where Id = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[UpdateUser]    Script Date: 10/15/2016 12:59:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateUser]
	-- Add the parameters for the stored procedure here
	@Id INT,
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),
	@Email VARCHAR(50),
	@Phone VARCHAR(50),
	@HomePhone VARCHAR(50) = NULL,
	@MobilePhone VARCHAR(50) = NULL,
	@Fax VARCHAR(50) = NULL,
	@Title VARCHAR(50) = NULL,
	@ReceiveUpdates bit,
	@FileName VARCHAR(100) = NULL,
	@PhysicalFileName VARCHAR(100) = NULL,
	@ModifiedBy VARCHAR(50) = NULL ,
	@ModifiedDate DateTime = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [User] 
	SET FirstName = @FirstName
	    ,LastName = @LastName
	    ,Email = @Email
	    ,Phone = @Phone
	    ,HomePhone = @HomePhone
	    ,MobilePhone = @MobilePhone
	    ,Fax = @Fax
	    ,Title = @Title
	    ,ReceiveUpdates = @ReceiveUpdates
	    ,[FileName] = @FileName
	    ,PhysicalFileName = @PhysicalFileName
	    ,ModifiedBy = @ModifiedBy
	    ,ModifiedDate = @ModifiedDate
	Where Id = @Id
END

GO
USE [master]
GO
ALTER DATABASE [eCOMM] SET  READ_WRITE 
GO
