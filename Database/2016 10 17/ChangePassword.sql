USE [eCOMM]
GO
/****** Object:  StoredProcedure [dbo].[UpdateUser]    Script Date: 10/17/2016 11:27:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ChangePassword]
	-- Add the parameters for the stored procedure here
	@Id INT,
	@NewPassword VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [User] 
	SET Password = @NewPassword
	Where Id = @Id

	SELECT 1
END
