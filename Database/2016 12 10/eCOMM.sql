USE [master]
GO
/****** Object:  Database [eCOMM20161028]    Script Date: 12/10/2016 12:51:17 AM ******/
CREATE DATABASE [eCOMM20161028]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'eCOMM', FILENAME = N'C:\Users\Justine\eCOMM20161028.mdf' , SIZE = 3328KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'eCOMM_log', FILENAME = N'C:\Users\Justine\eCOMM20161028_1.LDF' , SIZE = 3520KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [eCOMM20161028] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [eCOMM20161028].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [eCOMM20161028] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [eCOMM20161028] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [eCOMM20161028] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [eCOMM20161028] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [eCOMM20161028] SET ARITHABORT OFF 
GO
ALTER DATABASE [eCOMM20161028] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [eCOMM20161028] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [eCOMM20161028] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [eCOMM20161028] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [eCOMM20161028] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [eCOMM20161028] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [eCOMM20161028] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [eCOMM20161028] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [eCOMM20161028] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [eCOMM20161028] SET  DISABLE_BROKER 
GO
ALTER DATABASE [eCOMM20161028] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [eCOMM20161028] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [eCOMM20161028] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [eCOMM20161028] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [eCOMM20161028] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [eCOMM20161028] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [eCOMM20161028] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [eCOMM20161028] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [eCOMM20161028] SET  MULTI_USER 
GO
ALTER DATABASE [eCOMM20161028] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [eCOMM20161028] SET DB_CHAINING OFF 
GO
ALTER DATABASE [eCOMM20161028] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [eCOMM20161028] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [eCOMM20161028] SET DELAYED_DURABILITY = DISABLED 
GO
USE [eCOMM20161028]
GO
/****** Object:  User [mac]    Script Date: 12/10/2016 12:51:18 AM ******/
CREATE USER [mac] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[DisplayType]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DisplayType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Code] [nvarchar](50) NULL,
 CONSTRAINT [PK_Table_4] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Option]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Option](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OptionName] [nvarchar](100) NOT NULL,
	[DisplayName] [nvarchar](100) NULL,
	[DisplayTypeID] [int] NULL,
	[DefaultValue] [nvarchar](250) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Table_5] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OptionSet]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OptionSet](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Table_3] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OptionSetOption]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OptionSetOption](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OptionSetID] [int] NOT NULL,
	[OptionID] [int] NOT NULL,
	[IsRequired] [bit] NOT NULL,
 CONSTRAINT [PK_Table_6] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OptionValue]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OptionValue](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OptionID] [int] NOT NULL,
	[Value] [nvarchar](100) NOT NULL,
	[MakeDefault] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[ColorHex] [nvarchar](50) NULL,
 CONSTRAINT [PK_Table_7] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Person]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Person](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Age] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NULL,
	[FirstName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[Password] [varchar](100) NOT NULL,
	[Phone] [varchar](50) NOT NULL,
	[HomePhone] [varchar](50) NULL,
	[MobilePhone] [varchar](50) NULL,
	[Fax] [varchar](50) NULL,
	[ReceiveUpdates] [bit] NULL,
	[FileName] [varchar](100) NULL,
	[PhysicalFileName] [varchar](100) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IsVerified] [bit] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_Table_U1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[OptionSetOption] ADD  DEFAULT ((0)) FOR [IsRequired]
GO
ALTER TABLE [dbo].[OptionValue] ADD  DEFAULT ((0)) FOR [MakeDefault]
GO
ALTER TABLE [dbo].[Option]  WITH CHECK ADD  CONSTRAINT [FK_Option_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Option] CHECK CONSTRAINT [FK_Option_CreatedBy]
GO
ALTER TABLE [dbo].[Option]  WITH CHECK ADD  CONSTRAINT [FK_Option_DisplayType] FOREIGN KEY([DisplayTypeID])
REFERENCES [dbo].[DisplayType] ([Id])
GO
ALTER TABLE [dbo].[Option] CHECK CONSTRAINT [FK_Option_DisplayType]
GO
ALTER TABLE [dbo].[Option]  WITH CHECK ADD  CONSTRAINT [FK_Option_ModifiedBy] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Option] CHECK CONSTRAINT [FK_Option_ModifiedBy]
GO
ALTER TABLE [dbo].[OptionSet]  WITH CHECK ADD  CONSTRAINT [FK_OptionSet_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[OptionSet] CHECK CONSTRAINT [FK_OptionSet_CreatedBy]
GO
ALTER TABLE [dbo].[OptionSet]  WITH CHECK ADD  CONSTRAINT [FK_OptionSet_ModifiedBy] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[OptionSet] CHECK CONSTRAINT [FK_OptionSet_ModifiedBy]
GO
ALTER TABLE [dbo].[OptionSetOption]  WITH CHECK ADD FOREIGN KEY([OptionSetID])
REFERENCES [dbo].[OptionSet] ([Id])
GO
ALTER TABLE [dbo].[OptionSetOption]  WITH CHECK ADD FOREIGN KEY([OptionID])
REFERENCES [dbo].[Option] ([Id])
GO
ALTER TABLE [dbo].[OptionValue]  WITH CHECK ADD FOREIGN KEY([OptionID])
REFERENCES [dbo].[Option] ([Id])
GO
/****** Object:  StoredProcedure [dbo].[ChangePassword]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ChangePassword]
	-- Add the parameters for the stored procedure here
	@Id INT,
	@NewPassword VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [User] 
	SET Password = @NewPassword
	Where Id = @Id

	SELECT 1
END

GO
/****** Object:  StoredProcedure [dbo].[CreateOption]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CreateOption]
	-- Add the parameters for the stored procedure here
	@OptionName NVARCHAR(100),
	@DisplayName VARCHAR(100) = NULL,
	@DefaultValue NVARCHAR(250) = NULL,
	@DisplayTypeID INT,
	@CreatedBy INT,
	@CreatedDate Datetime,
	@ModifiedBy INT,
	@ModifiedDate Datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert new user record
    INSERT INTO [Option]
    (
		[OptionName],
		[DisplayName],
		[DefaultValue],
		[DisplayTypeID],
		[CreatedBy],
		[CreatedDate],
		[ModifiedBy],
		[ModifiedDate]
    )
    VALUES
    (
		@OptionName,
		@DisplayName,
		@DefaultValue,
		@DisplayTypeID,
		@CreatedBy,
		@CreatedDate,
		@ModifiedBy,
		@ModifiedDate
    )
    
	SELECT 1
END
GO
/****** Object:  StoredProcedure [dbo].[CreatePerson]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CreatePerson]
	-- Add the parameters for the stored procedure here
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),
	@Age INT,
	@CreatedBy VARCHAR(50),
	@CreatedDate Datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert new person record
    INSERT INTO Person
    (
		FirstName,
		LastName,
		Age,
		CreatedBy,
		CreatedDate
    )
    VALUES
    (
		@FirstName,
		@LastName,
		@Age,
		@CreatedBy,
		@CreatedDate
    )
    
	
END


GO
/****** Object:  StoredProcedure [dbo].[CreateUser]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CreateUser]
	-- Add the parameters for the stored procedure here
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),
	@Email VARCHAR(50),
	@Password VARCHAR(100),
	@Phone VARCHAR(50),
	@CreatedBy VARCHAR(50),
	@CreatedDate Datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert new user record
    INSERT INTO [User]
    (
		[FirstName],
		[LastName],
		[Email],
		[Password],
		[Phone],
		[CreatedBy],
		[CreatedDate]
    )
    VALUES
    (
		@FirstName,
		@LastName,
		@Email,
		@Password,
		@Phone,
		@CreatedBy,
		@CreatedDate
    )
    
	SELECT 1
END


GO
/****** Object:  StoredProcedure [dbo].[DeletePerson]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeletePerson]
	@PersonId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DELETE 
    FROM Person
	WHERE id = @PersonId
END


GO
/****** Object:  StoredProcedure [dbo].[GetDisplayType]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetDisplayType]
	@ID int
AS
BEGIN
	SELECT ID, Name, Code FROM DisplayType
	WHERE ID = @ID
END
GO
/****** Object:  StoredProcedure [dbo].[GetDisplayTypes]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetDisplayTypes]
AS
BEGIN
	SELECT ID, Name, Code FROM DisplayType
		
END

GO
/****** Object:  StoredProcedure [dbo].[GetOptions]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetOptions]
@ID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Id
		   ,[OptionName]
		   ,[DisplayName]
		   ,[DisplayTypeID]
		   ,[DefaultValue]
		   ,[CreatedBy]
		   ,[CreatedDate]
		   ,[ModifiedBy]
		   ,[ModifiedDate]
	FROM [Option]
	WHERE CreatedBy = @ID
END

GO
/****** Object:  StoredProcedure [dbo].[GetPersonById]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetPersonById]
	-- Add the parameters for the stored procedure here
	@PersonId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT Id
		   ,FirstName
		   ,LastName
		   ,Age
		   ,CreatedBy
		   ,CreatedDate
	FROM Person
	WHERE Id = @PersonId
END


GO
/****** Object:  StoredProcedure [dbo].[GetPersonList]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetPersonList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Id
		   ,FirstName
		   ,LastName
		   ,Age
		   ,CreatedBy
		   ,CreatedDate
	FROM Person
END


GO
/****** Object:  StoredProcedure [dbo].[GetUserByEmail]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetUserByEmail]
	-- Add the parameters for the stored procedure here
	@Email VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT Id
		   ,FirstName
		   ,LastName
		   ,Email
		   ,Title
		   ,Phone
		   ,HomePhone
		   ,MobilePhone
		   ,Fax
		   ,ReceiveUpdates
		   ,FileName
		   ,PhysicalFileName
		   ,Password
		   ,IsVerified
	FROM [User]
	WHERE Email = @Email
END

GO
/****** Object:  StoredProcedure [dbo].[GetUserById]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetUserById]
	-- Add the parameters for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT Id
		   ,FirstName
		   ,LastName
		   ,Email
		   ,Title
		   ,Phone
		   ,HomePhone
		   ,MobilePhone
		   ,Fax
		   ,ReceiveUpdates
		   ,FileName
		   ,PhysicalFileName
		   ,Password
	FROM [User]
	WHERE Id = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[GetUserList]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetUserList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Id
		   ,FirstName
		   ,LastName
		   ,Email
		   ,Phone
	FROM [User]
END


GO
/****** Object:  StoredProcedure [dbo].[ResetPassword]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ResetPassword]
	@Email VARCHAR(50),
	@TempPassword VARCHAR(50)
AS
BEGIN
	IF ((SELECT COUNT(*) FROM [User] WHERE Email = @Email) > 0)
	BEGIN
		UPDATE [User]
		SET [Password] = @TempPassword
		WHERE Email = @Email
		
		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0
	END
END

GO
/****** Object:  StoredProcedure [dbo].[UpdatePerson]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdatePerson]
	-- Add the parameters for the stored procedure here
	@Id INT,
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),
	@Age INT,
	@ModifiedBy VARCHAR(50),
	@ModifiedDate DateTime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE Person 
	SET FirstName = @FirstName
	    ,LastName = @LastName
	    ,Age = @Age
	    ,ModifiedBy = @ModifiedBy
	    ,ModifiedDate = @ModifiedDate
	Where Id = @Id
END


GO
/****** Object:  StoredProcedure [dbo].[UpdateUser]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateUser]
	-- Add the parameters for the stored procedure here
	@Id INT,
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),
	@Email VARCHAR(50),
	@Phone VARCHAR(50),
	@HomePhone VARCHAR(50) = NULL,
	@MobilePhone VARCHAR(50) = NULL,
	@Fax VARCHAR(50) = NULL,
	@Title VARCHAR(50) = NULL,
	@ReceiveUpdates bit,
	@FileName VARCHAR(100) = NULL,
	@PhysicalFileName VARCHAR(100) = NULL,
	@ModifiedBy VARCHAR(50) = NULL ,
	@ModifiedDate DateTime = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [User] 
	SET FirstName = @FirstName
	    ,LastName = @LastName
	    ,Email = @Email
	    ,Phone = @Phone
	    ,HomePhone = @HomePhone
	    ,MobilePhone = @MobilePhone
	    ,Fax = @Fax
	    ,Title = @Title
	    ,ReceiveUpdates = @ReceiveUpdates
	    ,[FileName] = @FileName
	    ,PhysicalFileName = @PhysicalFileName
	    ,ModifiedBy = @ModifiedBy
	    ,ModifiedDate = @ModifiedDate
	Where Id = @Id
END


GO
/****** Object:  StoredProcedure [dbo].[VerifyUser]    Script Date: 12/10/2016 12:51:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[VerifyUser] 
	-- Add the parameters for the stored procedure here
	@Email VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	IF ((SELECT COUNT(*) FROM [User] WHERE Email = @Email) > 0)
	BEGIN
		UPDATE [User]
		SET IsVerified = 1
		WHERE Email = @Email
		
		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0
	END
END

GO
USE [master]
GO
ALTER DATABASE [eCOMM20161028] SET  READ_WRITE 
GO
