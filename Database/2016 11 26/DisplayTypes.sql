INSERT INTO DisplayType (Name, Code) VALUES
('Checkbox', 'Checkbox'),
('Color', 'Color'),
('Text Field', 'TextField'),
('Number Field', 'NumberField'),
('Multiple Choice', 'MultipleChoice')