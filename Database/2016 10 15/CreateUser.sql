USE [eCOMM]
GO
/****** Object:  StoredProcedure [dbo].[CreateUser]    Script Date: 10/15/2016 15:04:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[CreateUser]
	-- Add the parameters for the stored procedure here
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),
	@Email VARCHAR(50),
	@Password VARCHAR(100),
	@Phone VARCHAR(50),
	@CreatedBy VARCHAR(50),
	@CreatedDate Datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert new user record
    INSERT INTO [User]
    (
		[FirstName],
		[LastName],
		[Email],
		[Password],
		[Phone],
		[CreatedBy],
		[CreatedDate]
    )
    VALUES
    (
		@FirstName,
		@LastName,
		@Email,
		@Password,
		@Phone,
		@CreatedBy,
		@CreatedDate
    )
    
	SELECT 1
END

